<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Login and Logout routes
 */

Route::get(
  'login',
  'Auth\LoginController@showLoginForm'
)->name('login');

Route::post(
  'login',
  'Auth\LoginController@login');

Route::post(
  'logout',
  'Auth\LoginController@logout'
)->name('logout');

/**
 * General routes
 */

Route::get(
  '/',
  'MainController@showIndex'
)->name('home');

Route::get(
  'inventario/buscar',
  'FilterController@inventario'
)->name('filtrar.inventario');

Route::get(
  '/inventario',
  'MainController@showInventario'
)->name('inventario');

Route::get(
  '/historial/entradas',
  'MainController@showHistorialEntradas'
)->name('historial.entradas');

Route::get(
  '/historial/salidas',
  'MainController@showHistorialSalidas'
)->name('historial.salidas');

Route::any(
  'historial/entradas/filtrar',
  'MainController@filtrarHistorialEntradas'
)->name('filtrar.historial.entradas');

Route::any(
  'historial/salidas/filtrar',
  'MainController@filtrarHistorialSalidas'
)->name('filtrar.historial.salidas');

/**
 * Provider routes
 */

Route::post(
  'perfil/actualizar',
  'ProviderController@postActualizarPerfil'
)->name('provider.actualizar.perfil');

/**
 * Admin routes
 */

Route::prefix('admin')->group(function () {
  Route::get(
    'herramientas/importar',
    'MainController@showImportar'
  )->name('admin.importar');

  Route::get(
    'herramientas/exportar',
    'MainController@showExportar'
  )->name('admin.exportar');

  Route::get(
    'herramientas/editar/{id}',
    'MainController@showEditar'
  )->name('admin.editar');

  Route::get(
    'herramientas/exportar/{tabla}/{type}',
    'AdminController@getExportar'
  )->name('admin.exportar.archivo')->middleware('admin');

  Route::get(
    'usuarios',
    'MainController@showUsuarios'
  )->name('admin.usuarios');

  Route::get(
    'usuarios/edit/{id}',
    'MainController@showEditarUsuario'
  )->name('admin.usuarios.editar');

  Route::post(
    'herramientas/editar/usuario',
    'AdminController@postEditarContra'
  )->name('admin.post.editar.contra')->middleware('admin');

  Route::get(
    'usuarios/registrar',
    'MainController@showRegistrar'
  )->name('admin.usuarios.registrar');

  Route::post(
    'herramientas/editar/producto',
    'AdminController@postEditarItem'
  )->name('admin.post.editar.producto')->middleware('admin');

  Route::post(
    'registrar',
    'AdminController@postRegistrar'
  )->name('admin.post.registrar')->middleware('admin');

  Route::post(
    'importar/inventario',
    'AdminController@postImportar'
  )->name('admin.importar.inventario')->middleware('admin');
});

/*
// Auth::routes();
// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register')->middleware('authenticated');
Route::post('register', 'Auth\RegisterController@register')->middleware('authenticated');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
*/
