    $(document).ready(function() {
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                  search: "_INPUT_",
                searchPlaceholder: "Search records",
            },
            dom: 'Bfrtip',
            buttons: [
           'csv', 'excel',
            ]

        });
      $('.btn-csv').click(function(){
        $('.buttons-csv').trigger('click');
      });
      $('.btn-excel').click(function(){
        $('.buttons-excel').trigger('click');
      });
    });
