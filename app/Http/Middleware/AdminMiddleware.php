<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \Closure $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if($request->user() && !$request->user()->admin) {
      return redirect(route('home'))->with('warning', 'Acceso no autorizado.');
    }
    return $next($request);
  }
}
