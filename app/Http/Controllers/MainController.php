<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function showIndex()
  {
    return view('index', ['title' => 'Profile']);
  }

  public function showInventario()
  {
    if (Auth::user()->admin) {
      $products = Product::paginate(50);
      $select = DB::table('products')
        ->select('empresa')
        ->distinct()
        ->get();

      return view('inventario', compact('products', 'select'))
        ->with('title', 'Inventario');
    }

    $products = Product::where('proveedor_email', Auth::user()->email)->paginate(50);
    return view('inventario', compact('products'))
      ->with('title', 'Inventario');
  }

  public function showHistorialEntradas()
  {
    if(Auth::user()->admin) {
      $products = DB::table('historial_entradas')
        ->latest('fecha')
        ->paginate(30);

      return view('entradas', compact('products'))
        ->with('title', 'Historial Entradas');
    }

    $products = DB::table('historial_entradas')
      ->select(['modelo', 'series', 'fecha', 'arribo'])
      ->latest('fecha')
      ->where('proveedor_email', Auth::user()->email)
      ->paginate(30);

    return view('entradas', compact('products'))
      ->with('title', 'Historial Entradas');
  }

  public function filtrarHistorialEntradas(Request $request)
  {
    /*
     * Admin Filter
     */
    if(Auth::user()->admin) {
      if($request->fechainicio == '' || $request->fechafinal == '' ) {
        return redirect()->back()->with('warning', 'Favor de elegir ambas fechas');
      }

      $fechainicio = Carbon::createFromFormat('d/m/Y', $request->fechainicio)->format('Y-m-d');
      $fechafinal = Carbon::createFromFormat('d/m/Y', $request->fechafinal)->format('Y-m-d');

      $products = DB::table('historial_entradas')
        ->whereBetween('fecha', [$fechainicio, $fechafinal])
        ->orderBy('fecha', 'asc')
        ->paginate(30)
        ->setpath('');

      $products->appends([
        'fechainicio' => $request->fechainicio,
        'fechafinal' => $request->fechafinal
      ]);

      if (count($products) > 0) {
        return view('filtrado-entradas', compact('products'))
          ->with('title', 'Historial Entradas')
          ->with('filter', "{$request->fechainicio} - {$request->fechafinal}")
          ->with('total', $products->total());
      }

      return view('filtrado-entradas')
        ->with('title', 'Historial Entradas')
        ->with('message', 'No se han encontrado resultados');
    }

    /*
     * Provider Filter
     */
    if($request->fechainicio == '' || $request->fechafinal == '' ) {
      return redirect()->back()->with('warning', 'Favor de elegir ambas fechas');
    }

    $fechainicio = Carbon::createFromFormat('d/m/Y', $request->fechainicio)->format('Y-m-d');
    $fechafinal = Carbon::createFromFormat('d/m/Y', $request->fechafinal)->format('Y-m-d');

    $products = DB::table('historial_entradas')
      ->select(['modelo', 'series', 'fecha', 'arribo'])
      ->whereBetween('fecha', [$fechainicio, $fechafinal])
      ->orderBy('fecha', 'asc')
      ->paginate(30)
      ->setpath('');

    $products->appends([
      'fechainicio' => $request->fechainicio,
      'fechafinal' => $request->fechafinal
    ]);

    if(count($products) > 0) {
      return view('filtrado-entradas', compact('products'))
        ->with('title', 'Historial Entradas')
        ->with('filter', "{$request->fechainicio} - {$request->fechafinal}")
        ->with('total', $products->total());
    }

    return view('filtrado-entradas')
      ->with('title', 'Historial Entradas')
      ->with('message', 'No se han encontrado resultados');
  }

  public function showHistorialSalidas()
  {
    if(Auth::user()->admin) {
      $products = DB::table('historial_salidas')
        ->latest('fecha')
        ->paginate(30);

      return view('salidas', compact('products'))
        ->with('title', 'Historial Salidas');
    }

    $products = DB::table('historial_salidas')
      ->select(['modelo', 'series', 'fecha', 'num_pedido'])
      ->where('proveedor_email', Auth::user()->email)
      ->latest('fecha')
      ->paginate(30);

    return view('salidas', compact('products'))
      ->with('title', 'Historial Salidas');
  }

  public function filtrarHistorialSalidas(Request $request)
  {
    /*
     * Admin Filter
     */
    if (Auth::user()->admin) {
      if($request->fechainicio == '' || $request->fechafinal == '' ) {
        return redirect()->back()->with('warning', 'Favor de elegir ambas fechas');
      }

      $fechainicio = Carbon::createFromFormat('d/m/Y', $request->fechainicio)->format('Y-m-d');
      $fechafinal = Carbon::createFromFormat('d/m/Y', $request->fechafinal)->format('Y-m-d');

      $products = DB::table('historial_salidas')
        ->whereBetween('fecha', [$fechainicio, $fechafinal])
        ->orderBy('fecha', 'asc')
        ->paginate(30)
        ->setpath('');

      $products->appends([
        'fechainicio' => $request->fechainicio,
        'fechafinal' => $request->fechafinal
      ]);

      if (count($products) > 0) {
        return view('filtrado-salidas', compact('products'))
          ->with('title', 'Historial Salidas')
          ->with('filter', "{$request->fechainicio} - {$request->fechafinal}")
          ->with('total', $products->total());
      }

      return view('filtrado-salidas')
        ->with('title', 'Historial Salidas')
        ->with('message', 'No se han encontrado resultados');
    }

    /*
     * Provider Filter
     */

    if($request->fechainicio == '' || $request->fechafinal == '' ) {
      return redirect()->back()->with('warning', 'Favor de elegir ambas fechas');
    }

    $fechainicio = Carbon::createFromFormat('d/m/Y', $request->fechainicio)->format('Y-m-d');
    $fechafinal = Carbon::createFromFormat('d/m/Y', $request->fechafinal)->format('Y-m-d');

    $products = DB::table('historial_salidas')
      ->whereBetween('fecha', [$fechainicio, $fechafinal])
      ->orderBy('fecha', 'asc')
      ->paginate(30)
      ->setpath('');

    $products->appends([
      'fechainicio' => $request->fechainicio,
      'fechafinal' => $request->fechafinal
    ]);

    if (count($products) > 0) {
      return view('filtrado-salidas', compact('products'))
        ->with('title', 'Historial Salidas')
        ->with('filter', "{$request->fechainicio} - {$request->fechafinal}")
        ->with('total', $products->total());
    }

    return view('filtrado-salidas')
      ->with('title', 'Historial Salidas')
      ->with('message', 'No se han encontrado resultados');
  }

  public function showImportar()
  {
    if(Auth::user()->admin) {
      return view('admin.importar', ['title' => 'Importar']);
    }
    return redirect()->route('home', [], 301);
  }

  public function showExportar()
  {
    if(Auth::user()->admin) {
      return view('admin.exportar', ['title' => 'Exportar']);
    }
    return redirect()->route('home', [], 301);
  }

  public function showUsuarios()
  {
    if (Auth::user()->admin) {
      $usuarios = DB::table('users')
        ->paginate(15);

      return view('admin.todos', compact('usuarios'))
        ->with('title', 'Usuarios');
    }
    return redirect()->route('home', [], 301);
  }

  public function showEditarUsuario($id)
  {
    if (Auth::user()->admin) {
      $user = User::find($id);
      return view('admin.editarUsuario', ['user' => $user])
        ->with('title', 'Editar');
    }
    return redirect()->route('home', [], 301);
  }

  public function showRegistrar()
  {
    if(Auth::user()->admin) {
      return view('admin.register', ['title' => 'Añadir Usuario']);
    }
    return redirect()->route('home', [], 301);
  }

  public function showEditar($id)
  {
    if(Auth::user()->admin) {
      $product = Product::where('id', $id)->first();
      return view('admin.editarItem', ['product' => $product])
        ->with('title', 'Editar');
    }
    return redirect()->route('home', [], 301);
  }
}
