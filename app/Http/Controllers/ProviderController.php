<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProviderController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function postActualizarPerfil(Request $request)
  {
    $user = Auth::user();
    if ($request->password != null) {
      $this->validate($request, [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255',
        'empresa' => 'required|string|max:100',
        'telefono' => 'required|string|max:12',
        'password' => 'required|string|min:6|confirmed'
      ]);

      $updated = $user->update($request->all());
      if ($updated) {
        return redirect()->back()->with('success', 'info updated successfully');
      }
      return redirect()->back()->with('error', 'info could not be updated');
    }

    $this->validate($request, [
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255',
      'empresa' => 'required|string|max:100',
      'telefono' => 'required|string|max:12'
    ]);

    $updated = $user->update($request->except(['password', '_token', 'password_confirmation']));
    if ($updated) {
      return redirect()->back()->with('success', 'info updated successfully');
    }
    return redirect()->back()->with('error', 'info could not be updated');
  }
}
