<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
  public function escaneo(Request $request)
  {
    $now = Carbon::now()->toDateTimeString();
    // change url
    $url = 'http://prologistics-scanner.test/';

    if ($request->accion == 'entrada') {
      $data = array();
      foreach ($request->series as $serie) {
        if ($serie !== null) {
          array_push($data, [
            'ubicacion' => $request->ubicacion,
            'modelo' => $request->modelo,
            'marca' => $request->marca,
            'series' => $serie,
            'empresa' => $request->empresa,
            'proveedor_email' => $request->proveedor_email
          ]);
        }
      }

      try {
        $inventario = Product::insert($data);
      } catch (\Exception $exception) {
        echo $exception->getMessage();
      }

      if ($inventario) {
        $data = array();
        foreach ($request->series as $serie) {
          if ($serie !== null) {
            array_push($data, [
              'ubicacion' => $request->ubicacion,
              'modelo' => $request->modelo,
              'series' => $serie,
              'empresa' => $request->empresa,
              'marca' => $request->marca,
              'proveedor_email' => $request->proveedor_email,
              'fecha' => $now,
              'arribo' => $request->arribo
            ]);
          }
        }

        try {
          $entrada = DB::table('historial_entradas')->insert($data);
        } catch (\Exception $exception) {
          echo $exception->getMessage();
        }
      }

      if ($entrada && $inventario) {
        return '<meta http-equiv="refresh" content="2;url=' . $url . 'success.html" />';
      }

      return '<meta http-equiv="refresh" content="2;url=' . $url . 'fail.html" />';
    } else {
      $series = array();
      foreach ($request->series as $serie) {
        if ($serie !== null) {
          array_push($series, $serie);
        }
      }

      $inventario = Product::where('modelo', $request->modelo)
        ->whereIn('series', $series)
        ->delete();

      if ($inventario) {
        $data = array();
        foreach ($request->series as $serie) {
          if ($serie !== null) {
            array_push($data, [
              'ubicacion' => $request->ubicacion,
              'modelo' => $request->modelo,
              'series' => $serie,
              'empresa' => $request->empresa,
              'marca' => $request->marca,
              'proveedor_email' => $request->proveedor_email,
              'fecha' => $now,
              'num_pedido' => $request->num_pedido
            ]);
          }
        }

        try {
          $salida = DB::table('historial_salidas')->insert($data);
        } catch (\Exception $exception) {
          echo $exception->getMessage();
        }
      } else {
        return '<meta http-equiv="refresh" content="2;url=' . $url . 'fail.html" />';
      }

      if ($salida && $inventario) {
        return '<meta http-equiv="refresh" content="2;url=' . $url . 'success.html" />';
      }

      return '<meta http-equiv="refresh" content="2;url=' . $url . 'fail.html" />';
    }
  }
}
