<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\Filter;
use Spatie\QueryBuilder\QueryBuilder;

class FilterController extends Controller
{
  public function inventario()
  {
    $select = DB::table('products')
      ->select('empresa')
      ->distinct()
      ->get();

    if (auth()->user()->admin) {
      if (request()->input('filter.ubicacion') == '' && request()->input('filter.proveedor') == ''
        && request()->input('filter.modelo') == '' && request()->input('filter.series') == '') {
        return redirect()->back()->with('warning', 'Favor de llenar uno de los campos');
      }

      $products = QueryBuilder::for(Product::class)
        ->defaultSort('-created_at')
        ->allowedFilters(['modelo', 'series', 'ubicacion', Filter::exact('proveedor', 'empresa'),])
        ->paginate(30, ['*'], 'pagina')
        ->setPath('');

      $products->appends(request()->all());

      if (count($products) > 0) {
        return view('filtrado', compact('products', 'select'))
          ->with('title', 'Inventario')
          ->with('filter', true);
      }
      return view('filtrado', compact('select'))
        ->with('title', 'Inventario')
        ->with('message', 'No se han encontrado resultados');
    }

    if (request()->input('filter.modelo') == '' && request()->input('filter.series') == '') {
      return redirect()->back()->with('warning', 'Favor de llenar uno de los campos');
    }

    $products = QueryBuilder::for(Product::class)
      ->defaultSort('-created_at')
      ->allowedFilters(['modelo', 'series',])
      ->paginate(30, ['*'], 'pagina')
      ->setPath('');

    $products->appends(request()->all());

    if (count($products) > 0) {
      return view('filtrado', compact('products'))
        ->with('title', 'Inventario')
        ->with('filter', true);
    }
    return view('filtrado', compact('select'))
      ->with('title', 'Inventario')
      ->with('message', 'No se han encontrado resultados');
  }
}
