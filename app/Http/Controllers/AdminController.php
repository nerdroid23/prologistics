<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Carbon\Carbon;
use Excel;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function postRegistrar(Request $request)
  {
    $this->validate(request(), [
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'empresa' => 'required|string|max:100',
      'telefono' => 'required|string|max:12',
      'password' => 'required|string|min:6|confirmed'
    ]);

    $user = User::create(request(['name', 'email', 'empresa', 'telefono', 'password'
    ]));

    if ($user) {
      return redirect()->back()->with('success', 'user created');
    }
    return redirect()->back()->with('error', 'could not create user');
  }

  public function postImportar(Request $request)
  {
    if ($request->tabla == "") {
      return redirect()->back()->with('error', 'Selecciona una tabla');
    }

    $this->validate($request, [
      'csv' => 'required|file|mimes:csv,txt|max:2048'
    ]);

    if ($request->tabla == 'inventario') {
      if ($request->hasFile('csv')) {
        $path = $request->file('csv')->getRealPath();
        $data = \Excel::load($path)->get()->toArray();
        if (count($data)) {
          $now = Carbon::now()->toDateTimeString();
          foreach ($data as $key => $value) {
            $products_list[] = [
              'ubicacion' => $value['ubicacion'],
              'modelo' => $value['modelo'],
              'marca' => $value['marca'],
              'series' => $value['series'],
              'empresa' => $value['empresa'],
              'proveedor_email' => $value['proveedor_email'],
              'created_at' => $now,
              'updated_at' => $now
            ];
          }

          if (!empty($products_list)) {
            foreach (array_chunk($products_list, 1500) as $products) {
              $inserted = Product::insert($products);
            }
          }

          if ($inserted) {
            return redirect()->back()->with('success', 'records successfully imported');
          } else {
            return redirect()->back()->with('error', 'records could not be imported');
          }
        }
      } else {
        return redirect()->back()->with('error', 'file was not imported');
      }
    }

    if ($request->tabla == 'entradas') {
      if ($request->hasFile('csv')) {
        $path = $request->file('csv')->getRealPath();
        $data = \Excel::load($path)->get()->toArray();
        if (count($data)) {
          $now = Carbon::now()->toDateTimeString();
          foreach ($data as $key => $value) {
            $fecha = Carbon::createFromFormat('Y-m-d', $value['fecha'])
              ->format('Y-m-d h:m:s');
            $entradas_list[] = [
              'ubicacion' => $value['ubicacion'],
              'modelo' => $value['modelo'],
              'series' => $value['series'],
              'empresa' => $value['empresa'],
              'marca' => $value['marca'],
              'proveedor_email' => $value['proveedor_email'],
              'fecha' => $fecha,
              'arribo' => $value['arribo']
            ];

            $inventario_list[] = [
              'ubicacion' => $value['ubicacion'],
              'modelo' => $value['modelo'],
              'marca' => $value['marca'],
              'series' => $value['series'],
              'empresa' => $value['empresa'],
              'proveedor_email' => $value['proveedor_email'],
              'created_at' => $now,
              'updated_at' => $now
            ];
          }
          DB::beginTransaction();
          if (!empty($entradas_list)) {
            foreach (array_chunk($entradas_list, 1500) as $products) {
              $entradas = DB::table('historial_entradas')->insert($products);
            }
          }

          if (!empty($inventario_list)) {
            foreach (array_chunk($inventario_list, 1500) as $products) {
              $inventario = Product::insert($products);
            }
          }

          if ($entradas && $inventario) {
            DB::commit();
            return redirect()->back()->with('success', 'records successfully imported');
          } else {
            DB::rollBack();
            return redirect()->back()->with('error', 'records could not be imported');
          }
        }
      } else {
        return redirect()->back()->with('error', 'file was not imported');
      }
    }

    if ($request->tabla == 'salidas') {
      if ($request->hasFile('csv')) {
        $path = $request->file('csv')->getRealPath();
        $data = \Excel::load($path)->get()->toArray();
        if (count($data)) {
          $now = Carbon::now()->toDateTimeString();
          foreach ($data as $key => $value) {
            $fecha = Carbon::createFromFormat('Y-m-d', $value['fecha'])
              ->format('Y-m-d h:m:s');
            $salidas_list[] = [
              'ubicacion' => 'n/a',
              'modelo' => $value['modelo'],
              'series' => $value['series'],
              'empresa' => $value['empresa'],
              'marca' => 'n/a',
              'proveedor_email' => $value['proveedor_email'],
              'fecha' => $fecha,
              'num_pedido' => $value['pedido']
            ];

            $series_list[] = $value['series'];
          }

          if (!empty($salidas_list) && !empty($series_list)) {

            $delete = DB::table('products')
              ->whereIn('series', $series_list)
              ->get(['series'])->toArray();
            $delete = array_map(function($item) {
              return $item->series;
            }, $delete);

//            dd(array_unique($delete));

            foreach (array_chunk($salidas_list, 1500) as $products) {
              foreach ($products as $product) {
                if (in_array($product['series'], $delete)) {
                  $inserted = DB::table('historial_salidas')->insert($product);
                }
              }
            }
//            dd(array_unique($insert));
            $deleted = DB::table('products')->whereIn('series', $delete)->delete();

            if ($inserted) {
              return redirect()->back()->with('success', 'records successfully imported');
            } else {
              return redirect()->back()->with('error', 'records could not be imported');
            }
          }
        }
      } else {
        return redirect()->back()->with('error', 'file was not imported');
      }
    }
    return redirect()->back()->with('error', 'an error occured, please try again');
  }

  public function getExportar($tabla, $type)
  {
    $now = Carbon::now()->toDateString();
    if ($tabla == 'inventario') {
      $products = Product::select('ubicacion', 'modelo', 'series', 'marca', 'empresa', 'proveedor_email')->get()
        ->toArray();
      return \Excel::create('Inventario_' . $now, function ($excel) use ($products) {
        $excel->sheet('Inventario Productos', function ($sheet) use ($products) {
          $sheet->fromArray($products);
        });
      })->download($type);
    }

    if ($tabla == 'entradas') {
      $products = DB::table('historial_entradas')
        ->select('ubicacion', 'modelo', 'series', 'marca', 'empresa', 'proveedor_email', 'fecha', 'arribo')
        ->get()
        ->toArray();
      $products = json_decode(json_encode($products), true);
      return \Excel::create('Entradas_' . $now, function ($excel) use ($products) {
        $excel->sheet('Historial Entradas', function ($sheet) use ($products) {
          $sheet->fromArray($products);
        });
      })->download($type);
    }

    if ($tabla == 'salidas') {
      $products = DB::table('historial_salidas')
        ->select('ubicacion', 'modelo', 'series', 'marca', 'empresa', 'proveedor_email', 'fecha', 'num_pedido')
        ->get()
        ->toArray();
      $products = json_decode(json_encode($products), true);
      return \Excel::create('Salidas_' . $now, function ($excel) use ($products) {
        $excel->sheet('Historial Salidas', function ($sheet) use ($products) {
          $sheet->fromArray($products);
        });
      })->download($type);
    }
  }

  public function postEditarItem(Request $request)
  {
    $this->validate($request, [
      'id' => 'required|numeric',
      'modelo' => 'required|string|max:80',
      'series' => 'required|string|max:80',
      'marca' => 'required|string|max:80',
      'ubicacion' => 'required|string|max:80',
      'empresa' => 'required|string|max:80',
      'proveedor_email' => 'required|string|email|max:250',
      'comentarios' => 'string|max:250'
    ]);

    $updated = Product::where('id', $request->id)->update($request->except(['_token']));

    if ($updated) {
      return redirect()->back()->with('success', 'info updated successfully');
    }
    return redirect()->back()->with('error', 'info could not be updated');
  }

  public function postEditarContra(Request $request)
  {
    $this->validate($request, [
      'id' => 'required|numeric',
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255',
      'empresa' => 'required|string|max:100',
      'telefono' => 'required|string|max:12',
      'password' => 'required|string|min:6|confirmed'
    ]);

    $updated = User::where('id', $request->id)->update([
      'password' => Hash::make($request->password)
    ]);
    if ($updated) {
      return redirect()->back()->with('success', 'password updated successfully');
    }
    return redirect()->back()->with('error', 'password could not be updated');
  }
}
