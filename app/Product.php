<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $fillable = [
    'ubicacion','modelo','series', 'marca' ,'empresa','proveedor_email', 'comentarios'
  ];
}
