<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name') }} | {{ $title }}</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome CSS-->
  <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
  <!-- Fontastic Custom icon font-->
  <link rel="stylesheet" href="{{ asset('css/fontastic.css') }}">
  <!-- Google fonts - Roboto -->
  <!-- jQuery Circle-->
  <link rel="stylesheet" href="{{ asset('css/grasp_mobile_progress_circle-1.0.0.min.css') }}">
  <!-- Custom Scrollbar-->
  <link rel="stylesheet" href="{{ asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') }}">
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="{{ asset('css/style.default.css') }}" id="theme-stylesheet">
  <!-- Custom stylesheet - for your changes-->
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
  <!-- datepicker css -->
  <link rel="stylesheet" href="{{ asset('vendor/datepicker/datepicker.css') }}">
  <!-- Favicon-->
  <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
</head>
<body>
  <!-- Side Navbar -->
  <nav class="side-navbar">
    <div class="side-navbar-wrapper">
      <!-- Sidebar Header    -->
      <div class="sidenav-header d-flex align-items-center justify-content-center">
        <!-- User Info-->
        <div class="sidenav-header-inner text-center">
          <img src="{{ asset('img/namelogo.svg') }}" width="100%">
        </div>
      </div>
      <!-- Small Brand information, appears on minimized sidebar-->
      <div class="sidenav-header-logo"><a href="{{ route('home') }}" class="brand-small text-center">
          <div class="box"><img src="{{ asset('img/logoicon.svg') }}" width="100%"></div>
        </a></div>
    </div>
    <!-- Sidebar Navigation Menus-->
    <div class="main-menu">
      <h5 class="sidenav-heading">Menu</h5>
      <ul id="side-main-menu" class="side-menu list-unstyled">
        <li>
          <a href="{{ route('home') }}">
            <i class="icon-home"></i>
            Perfil
          </a>
        </li>
        <li>
          <a href="{{ route('inventario') }}">
            <i class="icon-grid"></i>
            Inventario
          </a>
        </li>
        <li>
          <a href="#Historial" aria-expanded="false"
             data-toggle="collapse">
            <i class="icon-form"></i>
            Historial
          </a>
          <ul id="Historial" class="collapse list-unstyled ">
            <li>
              <a href="{{ route('historial.entradas') }}">Entradas</a>
            </li>
            <li>
              <a href="{{ route('historial.salidas') }}">Salidas</a>
            </li>
          </ul>
        </li>
        @if(Auth::user()->admin)
          <li>
            <a href="#Herramientas" aria-expanded="false" data-toggle="collapse">
              <i class="icon-pencil-case"></i>
              Herramientas
            </a>
            <ul id="Herramientas" class="collapse list-unstyled ">
              <li>
                <a href="{{ route('admin.importar') }}">Importar</a>
              </li>
              <li>
                <a href="{{ route('admin.exportar') }}">Exportar</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="#Usuario" aria-expanded="false" data-toggle="collapse">
              <i class="icon-user"></i>
              Usuario
            </a>
            <ul id="Usuario" class="collapse list-unstyled">
              <li>
                <a href="{{ route('admin.usuarios.registrar') }}">Añadir Usuario</a>
              </li>
              <li>
                <a href="{{ route('admin.usuarios') }}">Ver Usuarios</a>
              </li>
            </ul>
          </li>
        @endif
      </ul>
    </div>
  </nav>
  <div class="page">
    <!-- navbar-->
    <header class="header">
      <nav class="navbar">
        <div class="container-fluid">
          <div class="navbar-holder d-flex align-items-center justify-content-between">
            <div class="navbar-header">
              <a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i>
              </a>
            </div>
            
            <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
              
              <!--avatar-->
              <!--<div class="media">
                <div class="align-self-center">
                  <h5 class="mt-0">{{ Auth::user()->name }}</h5>
                </div>
                <img class="ml-1 rounded-circle" src="{{ asset('img/avatar-1.jpg') }}">
              </div>-->
              <!-- Log out-->
              <li class="nav-item">
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                   class="nav-link logout">
                  <span class="d-none d-sm-inline-block">Logout</span>
                  <i class="fa fa-sign-out"></i>
                </a>
              </li>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </ul>
          </div>
        </div>
      </nav>
    </header>
    
    <main>
      @yield('main')
    </main>
    
    
    <!-- Statistics Section-->
    <footer class="main-footer">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <p>{{ config('app.name') }} &copy; {{ date('Y') }}</p>
          </div>
        </div>
      </div>
    </footer>
  </div>
  <!-- JavaScript files-->
  <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/popper.js/umd/popper.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/grasp_mobile_progress_circle-1.0.0.min.js') }}"></script>
  <script src="{{ asset('vendor/jquery.cookie/jquery.cookie.js') }}"></script>
  <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
  <script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
  <script src="{{ asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
  <script src="{{ asset('js/charts-home.js') }}"></script>
  <script src="{{ asset('vendor/datepicker/datepicker.js') }}"></script>
  <script src="{{ asset('vendor/datepicker/datepicker.es-ES.js') }}"></script>
  <!-- Main File-->
  <script src="{{ asset('js/front.js') }}"></script>
  <script>
    $('[data-toggle="datepicker"]').datepicker({
      autoHide: true,
      language: 'es-ES',
      weekStart: 0
    });
  </script>
</body>
</html>