{{-- Todo: add message for empty date fields --}}
{{-- Todo: add red border around date fields when empty  --}}
@extends('layouts.master')
@section('main')
  @if(Auth::user()->admin)
    <div class="container">
      @include('partials.alert')
      <div class="d-flex flex-row justify-content-end">
        <form action="{{ route('filtrar.historial.entradas') }}" method="post"
              class="form-inline">
          @csrf
          <div class="col my-3 pl-0">
            <div class="input-group">
              <input name="fechainicio" type="text" class="form-control form-control-sm"
                     placeholder="Fecha Inicio" data-toggle="datepicker" autocomplete="off">
              <div class="input-group-append">
                <button class="btn btn-sm btn-outline-secondary" type="button" disabled>
                  <i class="fa fa-calendar"></i>
                </button>
              </div>
            </div>
          </div>
          <div class="col my-3 pl-0">
            <div class="input-group">
              <input name="fechafinal" type="text" class="form-control form-control-sm"
                     placeholder="Fecha Final" data-toggle="datepicker" autocomplete="off">
              <div class="input-group-append">
                <button class="btn btn-sm btn-outline-secondary" type="button" disabled>
                  <i class="fa fa-calendar"></i>
                </button>
              </div>
            </div>
          </div>
          <div class="col-auto my-3 pr-0">
            <button class="btn btn-sm btn-primary" type="submit">Submit</button>
          </div>
        </form>
      </div>
    </div>
  
    <div class="container">
      <div class="row">
        <div class="col">
          <table class="table table-hover table-responsive-sm">
            <thead class="thead-dark">
            <tr>
              <th scope="col">Modelo</th>
              <th scope="col">Serie</th>
              <th scope="col">Marca</th>
              <th scope="col">Ubicación</th>
              <th scope="col">Proveedor</th>
              <th scope="col">Fecha</th>
              <th scope="col">Arribo</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
              <tr>
                <th scope="row">{{ $product->modelo }}</th>
                <td>{{ $product->series }}</td>
                <td>{{ $product->marca }}</td>
                <td>{{ $product->ubicacion }}</td>
                <td>{{ $product->empresa }}</td>
                <td>
                  {{ Carbon\Carbon::createFromFormat(
                    'Y-m-d H:i:s', $product->fecha
                    )->format('d/m/Y h:i A')
                  }}
                </td>
                <td>{{ $product->arribo }}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
        
          <div class="modal-footer">
            {{ $products->links() }}
          </div>
        </div>
      </div>
      <script>
        var pagination = document.querySelector("ul.pagination");
        pagination.classList.add("pagination-sm");
      </script>
    </div>
  @else
    <div class="container">
      @include('partials.alert')
      <div class="d-flex flex-row justify-content-end">
        <form action="{{ route('filtrar.historial.entradas') }}" method="post"
              class="form-inline">
          @csrf
          <div class="col my-3 pl-0">
            <div class="input-group">
              <input name="fechainicio" type="text" class="form-control form-control-sm"
                     placeholder="Fecha Inicio" data-toggle="datepicker" autocomplete="off">
              <div class="input-group-append">
                <button class="btn btn-sm btn-outline-secondary" type="button" disabled>
                  <i class="fa fa-calendar"></i>
                </button>
              </div>
            </div>
          </div>
          <div class="col my-3 pl-0">
            <div class="input-group">
              <input name="fechafinal" type="text" class="form-control form-control-sm"
                     placeholder="Fecha Final" data-toggle="datepicker" autocomplete="off">
              <div class="input-group-append">
                <button class="btn btn-sm btn-outline-secondary" type="button" disabled>
                  <i class="fa fa-calendar"></i>
                </button>
              </div>
            </div>
          </div>
          <div class="col-auto my-3 pr-0">
            <button class="btn btn-sm btn-primary" type="submit">Submit</button>
          </div>
        </form>
      </div>
    </div>
  
    <div class="container">
      <div class="row">
        <div class="col">
          <table class="table table-hover table-responsive-sm">
            <thead class="thead-dark">
            <tr>
              <th scope="col">Modelo</th>
              <th scope="col">Serie</th>
              <th scope="col">Marca</th>
              <th scope="col">Fecha</th>
              <th scope="col">Arribo</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
              <tr>
                <th scope="row">{{ $product->modelo }}</th>
                <td>{{ $product->series }}</td>
                <td>{{ $product->marca }}</td>
                <td>
                  {{ Carbon\Carbon::createFromFormat(
                    'Y-m-d H:i:s', $product->fecha
                    )->format('d/m/Y h:i A')
                  }}
                </td>
                <td>{{ $product->arribo }}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
        
          <div class="modal-footer">
            {{ $products->links() }}
          </div>
        </div>
      </div>
      <script>
        var pagination = document.querySelector("ul.pagination");
        pagination.classList.add("pagination-sm");
      </script>
    </div>
  @endif
@endsection