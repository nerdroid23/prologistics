@if(session('error'))
  <div class="alert alert-danger text-center text-bold mt-3">
    {{ session('error') }}
  </div>
@endif

@if(session('warning'))
  <div class="alert alert-warning text-center text-bold mt-3">
    {{ session('warning') }}
  </div>
@endif

@if(session('success'))
  <div class="alert alert-success text-center text-bold mt-3">
    {{ session('success') }}
  </div>
@endif