@extends('layouts.master')
@section('main')
  <div class="container mt-5">
    <div class="row">
      <div class="col">
        <table class="table table-hover">
          <thead class="thead-dark">
          <tr>
            <th scope="col">User Name</th>
            <th scope="col">Empresa</th>
            <th scope="col">Télefono</th>
            <th scope="col">Email</th>
            <th scope="col">Acción</th>
          </tr>
          </thead>
          <tbody>
          @foreach($usuarios as $usuario)
            <tr>
              <th scope="row">{{ $usuario->name }}</th>
              <td>{{ $usuario->empresa }}</td>
              <td>{{ $usuario->telefono }}</td>
              <td>{{ $usuario->email }}</td>
              <td>
                <a href="{{ route('admin.usuarios.editar', $usuario->id) }}"><i class="fa fa-pencil"></i></a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="modal-footer">
          {{ $usuarios->links() }}
        </div>
      </div>
    </div>
    <script>
      var pagination = document.querySelector("ul.pagination");
      pagination.classList.add("pagination-sm");
    </script>
  </div>
@endsection