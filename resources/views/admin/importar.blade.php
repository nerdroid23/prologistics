@extends('layouts.master')
@section('main')
  <div class="container">
    <div class="mt-3">
      @include('partials.alert')
    </div>
    <div class="row justify-content-center">
      <div class="col-10 pt-3">
        <h2 class="mt-3">IMPORTAR ARCHIVOS</h2>
        <p>Puede importar datos de un archivo en csv y, a continuación, sus datos estarán en la platforma,
          esto lo ayudara a tener control mas detallado de sus datos en la empresa</p>
        <form action="{{ route('admin.importar.inventario') }}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            <div class="d-flex d-inline-flex pull-right">
              <div class="col my-2 pr-0">
                <div class="input-group">
                  <label for="importar" class="pt-1 pr-2">Seleccionar tabla:</label>
                  <select {{ session('error') ? 'style=border-color:red;' : '' }}
                      class="form-control-sm" name="tabla" id="importar">
                    <option value="" selected>--</option>
                    <option value="inventario">Inventario</option>
                    <option value="entradas">Entradas</option>
                    <option value="salidas">Salidas</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="custom-file">
            <input type="file" class="custom-file-input{{ $errors->has('csv') ? ' is-invalid' : '' }}"
                   name="csv" id="customFileLang" lang="es">
            <label class="custom-file-label" for="customFileLang">Seleccionar Archivo</label>
            @if ($errors->has('csv'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('csv') }}</strong>
              </span>
            @endif
          </div>
          <div class="d-flex d-inline-flex pull-right justify-content-end my-3">
            <button type="submit" class="btn btn-success">Importar</button>
          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
@endsection