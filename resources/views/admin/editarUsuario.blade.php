@extends('layouts.master')
@section('main')
  <div class="container">
    <div class="mt-3">
      @include('partials.alert')
    </div>
    <div class="col-md mt-5" id="info-margin">
      <h1>Editar Contraseña Usuario</h1>
      <form action="{{ route('admin.post.editar.contra') }}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $user->id }}">
        <div class="form-group">
          <label for="username">User Name:</label>
          <input type="text" name="name" id="username"
                 class="form-control form-control-sm{{ $errors->has('name') ? ' is-invalid' : '' }}"
                 value="{{ $user->name }}" readonly>
          @if($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
          @endif
        </div>
        <div class="form-group">
          <label for="company">Empresa:</label>
          <input type="text" name="empresa" id="company"
                 class="form-control form-control-sm{{ $errors->has('empresa') ? ' is-invalid' : '' }}"
                 value="{{ $user->empresa}}" readonly>
          @if ($errors->has('empresa'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('empresa') }}</strong>
              </span>
          @endif
        </div>
        <div class="form-group">
          <label for="Telefono">Télefono:</label>
          <input type="number" name="telefono" id="Telefono"
                 class="form-control form-control-sm{{ $errors->has('telefono') ? ' is-invalid' : '' }}"
                 value="{{ $user->telefono }}" readonly>
          @if ($errors->has('telefono'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('telefono') }}</strong>
              </span>
          @endif
        </div>
        <div class="form-group">
          <label for="email">Email:</label>
          <input type="email" name="email" id="email"
                 class="form-control form-control-sm{{ $errors->has('email') ? ' is-invalid' : '' }}"
                 value="{{ $user->email }}" readonly>
          @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif
        </div>
        <div class="form-group">
          <label for="pwd">Password:</label>
          <input type="password" id="pwd" name="password"
                 class="form-control form-control-sm{{ $errors->has('password') ? ' is-invalid' : '' }}"
                 placeholder="Enter password">
          @if($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
          @endif
        </div>
        <div class="form-group">
          <label for="pwd-c">Confirm Password:</label>
          <input type="password" id="pwd-c"
                 class="form-control form-control-sm{{ $errors->has('password') ? ' is-invalid' : '' }}"
                 name="password_confirmation" placeholder="Confirm password">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-success btn-block">Guardar</button>
        </div>
      </form>
    </div>
  </div>
@endsection