@extends('layouts.master')
@section('main')
  <div class="container">
    <div class="mt-3">
      @include('partials.alert')
    </div>
    <div class="row justify-content-center mt-3">
      <div class="col-10" id="info-margin">
        <h1>Edición de Inventario</h1>
        <form action="{{ route('admin.post.editar.producto') }}" method="post">
          @csrf
          <input type="hidden" name="id" value="{{ $product->id }}">
          <div class="form-group">
            <label for="modelo">Modelo:</label>
            <input type="text" name="modelo" id="modelo"
                   class="form-control form-control-sm{{ $errors->has('modelo') ? ' is-invalid' : '' }}"
                   placeholder="Modelo:" value="{{ $product->modelo }}">
            @if ($errors->has('modelo'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('modelo') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group">
            <label for="serie">Serie:</label>
            <input type="text" name="series" id="serie"
                   class="form-control form-control-sm{{ $errors->has('series') ? ' is-invalid' : '' }}"
                   placeholder="Serie:" value="{{ $product->series }}">
            @if ($errors->has('series'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('series') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group">
            <label for="marca">Marca:</label>
            <input type="text" name="marca" id="marca"
                   class="form-control form-control-sm{{ $errors->has('marca') ? ' is-invalid' : '' }}"
                   placeholder="Serie:" value="{{ $product->marca }}">
            @if ($errors->has('marca'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('marca') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group">
            <label for="ubicacion">Ubicación:</label>
            <input type="text" name="ubicacion" id="ubicacion"
                   class="form-control form-control-sm{{ $errors->has('ubicacion') ? ' is-invalid' : '' }}"
                   placeholder="Ubicación" value="{{ $product->ubicacion }}">
            @if ($errors->has('ubicacion'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('ubicacion') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group">
            <label for="empresa">Proveedor:</label>
            <input type="text" name="empresa" id="empresa"
                   class="form-control form-control-sm{{ $errors->has('empresa') ? ' is-invalid' : '' }}"
                   placeholder="Proveedor:" value="{{ $product->empresa }}">
            @if ($errors->has('empresa'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('empresa') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group">
            <label for="provider-email">Correo Proveedor:</label>
            <input type="email" name="proveedor_email" id="provider-email"
                   class="form-control form-control-sm{{ $errors->has('proveedor_email') ? ' is-invalid' : '' }}"
                   placeholder="Correo Proveedor:" value="{{ $product->proveedor_email }}">
            @if ($errors->has('proveedor_email'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('proveedor_email') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group">
            <label for="comentarios">Comentarios:</label>
            <textarea type="email" name="comentarios" id="comentarios"
                      class="form-control form-control-sm{{ $errors->has('comentarios') ? ' is-invalid' : ''
                      }}">{{ $product->comentarios }}</textarea>
            @if ($errors->has('comentarios'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('comentarios') }}</strong>
              </span>
            @endif
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Guardar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection