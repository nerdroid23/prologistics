@extends('layouts.master')
@section('main')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-10">
        <h2 class="mt-4">EXPORTAR ARCHIVOS</h2>
        <p>Puede exportar los datos como una hoja de cálculos de Excel o un archivo en csv.</p>
        <div class="modal-footer">
          <div class="col-4">
            <div class="col text-center">
              <h3>Inventario</h3>
            </div>
            <a href="{{ route('admin.exportar.archivo', ['tabla' => 'inventario', 'type' => 'xlsx']) }}"
               class="btn btn-primary">Descargar Excel</a>
            <a href="{{ route('admin.exportar.archivo', ['tabla' => 'inventario', 'type' => 'csv']) }}"
               class="btn btn-primary">Descargar CSV</a>
          </div>
          <div class="col-4">
            <div class="col text-center">
              <h3>Historial Entradas</h3>
            </div>
            <a href="{{ route('admin.exportar.archivo', ['tabla' => 'entradas', 'type' => 'xlsx']) }}"
               class="btn btn-primary">Descargar Excel</a>
            <a href="{{ route('admin.exportar.archivo', ['tabla' => 'entradas', 'type' => 'csv']) }}"
               class="btn btn-primary">Descargar CSV</a>
          </div>
          <div class="col-4">
            <div class="col text-center">
              <h3>Historial Salidas</h3>
            </div>
            <a href="{{ route('admin.exportar.archivo', ['tabla' => 'salidas', 'type' => 'xlsx']) }}"
               class="btn btn-primary">Descargar Excel</a>
            <a href="{{ route('admin.exportar.archivo', ['tabla' => 'salidas', 'type' => 'csv']) }}"
               class="btn btn-primary">Descargar CSV</a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection