@extends('layouts.master')
@section('main')
  <div class="container">
    <div class="mt-3">
      @include('partials.alert')
    </div>
    <div class="row justify-content-center mt-3">
      <div class="col-10" id="info-margin">
        <h1>Añadir Usuarios</h1>
        <form method="post" action="{{ route('admin.post.registrar') }}">
          @csrf
          <div class="form-group">
            <label for="name">User Name:</label>
            <input type="text" class="form-control form-control-sm{{ $errors->has('name') ? ' is-invalid' : '' }}"
                   id="name" name="name" placeholder="Enter user name" value="{{ old('name') }}">
            @if ($errors->has('name'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group">
            <label for="empresa">Empresa:</label>
            <input type="text" class="form-control form-control-sm{{ $errors->has('empresa') ? ' is-invalid' : '' }}"
                   id="company" name="empresa" placeholder="Enter company" value="{{ old('empresa') }}">
            @if ($errors->has('empresa'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('empresa') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group">
            <label for="telefono">Télefono:</label>
            <input type="number" class="form-control form-control-sm{{ $errors->has('telefono') ? ' is-invalid' : '' }}"
                   id="Telefono" name="telefono" placeholder="Enter Telefono" value="{{ old('telefono') }}">
            @if ($errors->has('telefono'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('telefono') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control form-control-sm{{ $errors->has('email') ? ' is-invalid' : '' }}"
                   id="email" name="email" placeholder="Enter email" value="{{ old('email') }}">
            @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control form-control-sm{{ $errors->has('password') ? ' is-invalid' : '' }}"
                   id="pwd" name="password" placeholder="Enter password">
            @if($errors->has('password'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group">
            <label for="password-confirm">Confirm Password:</label>
            <input type="password" class="form-control form-control-sm{{ $errors->has('password') ? ' is-invalid' : '' }}" id="pwd-c"
                   placeholder="Confirm password" name="password_confirmation">
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Guardar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection