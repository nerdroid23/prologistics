@extends('layouts.master')
@section('main')
  <!-- Admin View -->
  @if(count($products) > 0)
    @if(auth()->user()->admin)
      <div class="container">
        @include('partials.alert')
        <div class="d-flex flex-row justify-content-end">
          <form action="{{ route('filtrar.inventario') }}" method="get"
                class="form-inline">
            <div class="col my-3 pl-0">
              <input class="form-control form-control-sm" type="text"
                     name="filter[modelo]" placeholder="Modelo:">
            </div>
            <div class="col my-3">
              <input class="form-control form-control-sm" type="text"
                     name="filter[series]" placeholder="Serie:">
            </div>
            <div class="col my-3 pl-0">
              <input class="form-control form-control-sm" type="text"
                     name="filter[ubicacion]" placeholder="Ubicación:">
            </div>
            <div class="col my-3 pl-0">
              <select name="filter[proveedor]" class="form-control form-control-sm"
                      id="inlineFormCustomSelect">
                <option value="" selected>Proveedor</option>
                @foreach($select as $sel)
                  <option value="{{ $sel->empresa }}">
                    {{ $sel->empresa }}
                  </option>
                @endforeach
              </select>
            </div>
            <div class="col-auto my-3 pr-0">
              <button class="btn btn-sm btn-primary"
                      type="submit">Submit
              </button>
            </div>
          </form>
        </div>
      </div>
      <div class="container">
        @if(isset($products))
          <div class="row">
            <div class="col">
              <table class="table table-hover table-responsive-sm">
                <thead class="thead-dark">
                <tr>
                  <th scope="col">Modelo</th>
                  <th scope="col">Serie</th>
                  <th scope="col">Marca</th>
                  <th scope="col">Ubicación</th>
                  <th scope="col">Proveedor</th>
                  <th scope="col">Acción</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                  <tr>
                    <th scope="row">{{ $product->modelo }}</th>
                    <td>{{ $product->series }}</td>
                    <td>{{ $product->marca }}</td>
                    <td>{{ $product->ubicacion }}</td>
                    <td>{{ $product->empresa }}</td>
                    <td>
                      <a href="{{ route('admin.editar', $product->id) }}">
                        <i class="fa fa-pencil"></i>
                      </a>
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="modal-footer">
                {{ $products->links() }}
              </div>
            </div>
          </div>
          <script>
            var pagination = document.querySelector("ul.pagination");
            pagination.classList.add("pagination-sm");
          </script>
        @endif
      </div>
    @else
      <!-- Provider View -->
      <div class="container">
        @include('partials.alert')
        <div class="d-flex flex-row justify-content-end">
          <form action="{{ route('filtrar.inventario') }}" method="get"
                class="form-inline">
            <div class="col-auto my-3">
              <input class="form-control" type="text" value="{{ request()->input('filter.modelo') }}"
                     name="filter[modelo]" placeholder="Modelo:">
            </div>
            <div class="col-auto my-3">
              <input class="form-control" type="text" value="{{ request()->input('filter.series') }}"
                     name="filter[series]" placeholder="Serie:">
            </div>
            <div class="col-auto my-3 pr-0">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col">
            <table class="table table-hover table-responsive-sm">
              <thead class="thead-dark">
              <tr>
                <th scope="col">Modelo</th>
                <th scope="col">Serie</th>
                <th scope="col">Marca</th>
              </tr>
              </thead>
              
              <tbody>
              @foreach($products as $product)
                <tr>
                  <th scope="row">{{ $product->modelo }}</th>
                  <td>{{ $product->series }}</td>
                  <td>{{ $product->marca }}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="modal-footer">
              {{ $products->links() }}
            </div>
          </div>
        </div>
        <script>
          var pagination = document.querySelector("ul.pagination");
          pagination.classList.add("pagination-sm");
        </script>
      </div>
    @endif
  @else
    <div class="container">
      <div class="row mt-5">
        <div class="col">
          <h1 class="text-bold text-center">El inventario esta vacio</h1>
        </div>
      </div>
    </div>
  @endif
@endsection