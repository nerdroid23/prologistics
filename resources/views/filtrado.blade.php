@extends('layouts.master')
@section('main')
  <!-- Admin View -->
  @if(auth()->user()->admin)
    <div class="container">
      @include('partials.alert')
      <div class="d-flex flex-row justify-content-end">
        <form action="{{ route('filtrar.inventario') }}" method="get"
              class="form-inline">
          <div class="col my-3 pl-0">
            <input class="form-control form-control-sm" type="text" value="{{ request()->input('filter.modelo') }}"
                   name="filter[modelo]" placeholder="Modelo:">
          </div>
          <div class="col-auto my-3">
            <input class="form-control form-control-sm" type="text" value="{{ request()->input('filter.series') }}"
                   name="filter[series]" placeholder="Serie:">
          </div>
          <div class="col my-3 pl-0">
            <input class="form-control form-control-sm" type="text" value="{{ request()->input('filter.ubicacion') }}"
                   name="filter[ubicacion]" placeholder="Ubicación:">
          </div>
          <div class="col my-3 pl-0">
            <select name="filter[proveedor]" class="form-control form-control-sm"
                    id="inlineFormCustomSelect">
              <option value="" selected>Proveedor</option>
              @foreach($select as $sel)
                <option value="{{ $sel->empresa }}"
                    {{ request()->input('filter.proveedor') == $sel->empresa ? 'selected' : '' }}
                >
                  {{ $sel->empresa }}
                </option>
              @endforeach
            </select>
          </div>
          <div class="col-auto my-3 pr-0">
            <button class="btn btn-sm btn-primary"
                    type="submit">Submit
            </button>
          </div>
        </form>
      </div>
      @if($filter)
        <div>
          <div class="d-flex d-inline-flex">
            <p class="text-muted text-small p-1">
              Total encontrado: <strong>{{ $products->total() }}</strong>
            </p>
          </div>
          <div class="d-flex d-inline-flex pull-right justify-content-end">
            <!--<p class="text-muted text-small p-1 mr-3">
              filtrado por <strong>{{-- $filter --}}</strong>
            </p>-->
            <p class="text-small">
              <a href="{{ route('inventario') }}" class="btn-outline-danger p-1">Borrar filtro(s)</a>
            </p>
          </div>
        </div>
      @endif
    </div>
    <div class="container">
      @if(isset($message))
        <div class="row">
          <div class="center mt-5">
            <img src="{{ asset('img/navegador.svg') }}"  class="rounded mx-auto d-block"
                 width="30%">
            <h3 class="text-center mt-5">{{ $message }}</h3>
          </div>
        </div>
      @endif
      @if(isset($products))
        <div class="row">
          <div class="col">
            <table class="table table-hover table-responsive-sm">
              <thead class="thead-dark">
              <tr>
                <th scope="col">Modelo</th>
                <th scope="col">Serie</th>
                <th scope="col">Marca</th>
                <th scope="col">Ubicación</th>
                <th scope="col">Proveedor</th>
                <th scope="col">Acción</th>
              </tr>
              </thead>
              <tbody>
              @foreach($products as $product)
                <tr>
                  <th scope="row">{{ $product->modelo }}</th>
                  <td>{{ $product->series }}</td>
                  <td>{{ $product->marca }}</td>
                  <td>{{ $product->ubicacion }}</td>
                  <td>{{ $product->empresa }}</td>
                  <td>
                    <a href="{{ route('admin.editar', $product->id) }}">
                      <i class="fa fa-pencil"></i></a>
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="modal-footer">
              {{ $products->links() }}
            </div>
          </div>
        </div>
        <script>
          var pagination = document.querySelector("ul.pagination");
          pagination.classList.add("pagination-sm");
        </script>
      @endif
    </div>
  @else
    <!-- Provider View -->
    <div class="container">
      @include('partials.alert')
      <div class="d-flex flex-row justify-content-end">
        <form action="{{ route('filtrar.inventario') }}" method="get"
              class="form-inline">
          <div class="col-auto my-3">
            <input class="form-control" type="text" value="{{ request()->input('filter.modelo') }}"
                   name="filter[modelo]" placeholder="Modelo:">
          </div>
          <div class="col-auto my-3">
            <input class="form-control" type="text" value="{{ request()->input('filter.series') }}"
                   name="filter[series]" placeholder="Serie:">
          </div>
          <div class="col-auto my-3 pr-0">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
      @if(isset($filter))
        <div>
          <div class="d-flex d-inline-flex">
            <p class="text-muted text-small p-1">
              Total encontrado: <strong>{{ $products->total() }}</strong>
            </p>
          </div>
          <div class="d-flex d-inline-flex pull-right justify-content-end">
            <p class="text-small">
              <a href="{{ route('inventario') }}" class="btn-outline-danger p-1">Borrar filtro(s)</a>
            </p>
          </div>
        </div>
      @endif
    </div>
    <div class="container">
      @if(isset($message))
        <div class="row">
          <div class="center mt-5">
            <img src="{{ asset('img/navegador.svg') }}"  class="rounded mx-auto d-block"
                 width="30%">
            <h3 class="text-center mt-5">{{ $message }}</h3>
          </div>
        </div>
      @endif
      @if(isset($products))
        <div class="row">
          <div class="col">
            <table class="table table-hover table-responsive-sm">
              <thead class="thead-dark">
              <tr>
                <th scope="col">Modelo</th>
                <th scope="col">Serie</th>
                <th scope="col">Marca</th>
              </tr>
              </thead>
              <tbody>
              @foreach($products as $product)
                <tr>
                  <th scope="row">{{ $product->modelo }}</th>
                  <td>{{ $product->series }}</td>
                  <td>{{ $product->Marca }}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="modal-footer">
              {{ $products->links() }}
            </div>
          </div>
        </div>
        <script>
          var pagination = document.querySelector("ul.pagination");
          pagination.classList.add("pagination-sm");
        </script>
      @endif
    </div>
  @endif
@endsection